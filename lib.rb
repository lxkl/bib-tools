# coding: utf-8
require "csv"
require "i18n"
require "nokogiri"

I18n.available_locales = [:en]
PICA_NS_URL = "info:srw/schema/5/picaXML-v1.0"
PICA_NS = {"pica" => PICA_NS_URL}

## generic accessors

def getdf(record, tag, occurrence: nil, is_repeatable: false)
  df = record.xpath("//pica:datafield[@tag='#{tag}']", PICA_NS).select do |x|
    ! occurrence || occurrence.include?(x["occurrence"])
  end.uniq(&:to_s)
  if ! is_repeatable && df.length > 1
    puts "WARNING: non-repeatable datafield repeated: #{tag}"
    puts format_record(record)
  end
  puts "WARNING: non-array class" unless df.class == Array
  df
end

def getsf_(df, code)
  df.xpath("pica:subfield[@code='#{code}']", PICA_NS).map(&:content)
end

def getsf(df, code, is_repeatable: false, upcase_fallback: false, allow_empty: true)
  sf = getsf_(df, code)
  if upcase_fallback && sf.empty?
    code = code.upcase
    sf = getsf_(df, code)
  end
  if ! allow_empty && sf.empty?
    puts "WARNING: unexpected empty subfield: #{code}"
    puts format_df(df)
  end
  if ! is_repeatable && sf.length > 1
    puts "WARNING: non-repeatable subfield repeated: #{code}"
    puts format_df(df)
  end
  puts "WARNING: non-array class" unless sf.class == Array
  sf
end

def getdfsf(record, tag, code, occurrence: nil, allow_empty: true, subfield_is_repeatable: false)
  df = getdf(record, tag, occurrence: occurrence)
  if df.empty?; res = [] else res = getsf(df.first, code, is_repeatable: subfield_is_repeatable); end
  if ! allow_empty && res.empty?
    puts "WARNING: unexpected empty subfield: #{code}"
    puts format_record(record)
  end
  res
end

## get specific information

def get_ppn(record)
  getdfsf(record, "003@", "0", allow_empty: false).first.rjust(9, "0")
end

def get_author(record)
  author = nil
  (getdf(record, "028A") + getdf(record, "028C", is_repeatable: true)).each do |df|
    author = getsf(df, "8").first
    unless author
      x = getsf(df, "a", upcase_fallback: true).first
      y = getsf(df, "d", upcase_fallback: true).first
      if x && y
        author = "#{x}, #{y}"
      elsif x && ! y
        author = "#{x}"
      elsif ! x && y
        author = "#{y}"
      end
    end
    author = getsf(df, "p", upcase_fallback: true).first unless author
    break if author
  end
  author
end

def get_title(record)
  title = nil
  (getdf(record, "021A") + getdf(record, "036C", occurrence: [nil, "00"])).each do |df|
    title_1 = getsf(df, "a").first
    title_2_list = getsf(df, "d", is_repeatable: true)
    if title_2_list.length == 1
      title_2 = title_2_list.first
    elsif title_2_list.length > 1
      title_2 = "#{title_2_list.first} [#{title_2_list[1..-1].join(" ")}]"
    else
      title_2 = nil
    end
    if title_1
      title = "#{title_1}"
      title += ": #{title_2}" if title_2
      break
    end
  end
  title
end

def get_edition(record)
  getdfsf(record, "032@", "a").first
end

def get_year(record)
  getdfsf(record, "011@", "a").first
end

def get_volume(super_df)
  volume = getsf(super_df, "l").first
  volume = getsf(super_df, "X", allow_empty: false).first unless volume && volume.match(/[0-9]/)
  volume
end

def get_copy_list(record)
  datafields = record.root.children.select { |x| x.name == "datafield" }
  datafields.map.with_index do |df, i|
    if df["tag"] == "209A" && ["00"].include?(getsf(df, "x").first) && ["8/60", "8/60a"].include?(getsf(df, "f").first)
      if datafields[i-1] && datafields[i-1]["tag"] == "208@" && ["df060", "df601"].include?(getsf(datafields[i-1], "b"))
        nil
      else
        { "signature" => getsf(df, "a").first,
          "special_location" => if datafields[i+1] && datafields[i+1]["tag"] == "209A"; then getsf(datafields[i+1], "a").first else nil end }
      end
    end
  end.compact
end

## format records for output

def format_df(df, compact: false)
  res = ""
  tag = df["tag"] || "????"
  occurrence = df["occurrence"]
  if compact
    if occurrence
      res += "%s/%s " % [tag, occurrence]
    else
      res += "%s    " % [tag]
    end
    df.xpath("pica:subfield", PICA_NS).each do |sf|
      code = sf["code"] || "?"
      res += "$%s%s" % [code, sf.content]
    end
    res += "\n"
  else
    is_first_line = true
    df.xpath("pica:subfield", PICA_NS).each do |sf|
      code = sf["code"] || "?"
      if is_first_line
        if occurrence
          res += "%s/%s %s %s\n" % [tag, occurrence, code, sf.content]
        else
          res += "%s    %s %s\n" % [tag, code, sf.content]
        end
        is_first_line = false
      else
        res += "        %s %s\n" % [code, sf.content]
      end
    end
  end
  res
end

def format_record(record, compact: false)
  record.xpath("//pica:datafield", PICA_NS).map { |df| format_df(df, compact: compact) }.join
end

## iterate over all records in a file

def foreach_record(picaxml_fn)
  Nokogiri::XML::Reader(File.open(picaxml_fn)).each do |node|
    if node.name == "record" && node.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT
      record = Nokogiri::XML.parse(node.outer_xml)
      yield record
    end
  end
end

## misc

def normalize_string_for_sorting(s, is_author: false, is_title: false)
  return "" unless s
  s = s.strip
  s = s.gsub(/ä/, "ae").gsub(/ö/, "oe").gsub(/ü/, "ue").gsub(/ß/, "ss").gsub(/Ä/, "Ae").gsub(/Ö/, "Oe").gsub(/Ü/, "Ue").gsub(/ø/, "oe").gsub(/Ø/, "Oe")
  s = I18n.transliterate(s)
  s = s.gsub(/\?/, "")
  if is_author
    s = s.gsub(/['`]/, "")
    s = s.strip
    s = s.sub(/^[\sa-z]+([A-Z])/, "\\1")
    s = s.gsub(/\b([a-zA-Z])\s+/, "\\1")
    s = s.sub(/^([^\s,]+),\s+(\S+).*/, "\\1, \\2")
    s = s.sub(/^([^\s,]+),\s+([^\s\.])+\..*/, "\\1, \\2.")
  end
  if is_title
    s = s.sub(/[^@]*@/, "")
  end
  s
end
